Setting up a new user's (user2) read-only permissions:

![](image_2022-11-22_01-28-53.png)

Output of parametrized HelloUser job (selected in blue at the bottom of the screen):

![](image_2022-11-22_01-39-42.png)

Cobertura was causing issues, so it was replaced with JaCoCo code-coverage tool:

![](image_2022-11-22_01-41-12.png)