The task clones the project and launches tests: 

![](image_2022-11-22_00-54-52.png)
![](image_2022-11-22_00-55-04.png)

Setting of build triggers:

![](image_2022-11-22_01-12-52.png)

Archiving required atifact as follows:

![](image_2022-11-22_00-57-10.png)
