To perform the same tasks as it was done previously on Linux node, the VMware virtual machine was used:

![](image_2022-11-23_17-55-05.png)
![](image_2022-11-23_17-57-15.png)
![](image_2022-11-23_17-55-51.png)

Job's workspace looks like this:

![](image_2022-11-23_17-45-48.png)

Job's configurations:

![](image_2022-11-23_17-54-06.png)
![](image_2022-11-23_17-54-12.png)
![](image_2022-11-23_17-54-32.png)
![](image_2022-11-23_17-54-42.png)

It archives artifact and shows the build's coverage graph as follows:

![](image_2022-11-23_17-48-15.png)

Result (last picture shows echoed parameter that is selected in blue):

![](image_2022-11-23_17-48-50.png)
![](image_2022-11-23_17-49-09.png)
![](image_2022-11-23_17-49-19.png)
![](image_2022-11-23_17-49-41.png)
