Changing the port to 8081 (selected in green):

![](image_2022-11-22_01-16-00.png)

Creating a node and running job only on that slave node:

![](image_2022-11-22_01-17-39.png)
![](image_2022-11-22_01-16-35.png)

Job Config History in action:

![](image_2022-11-22_01-48-23.png)

thinBackup configuration:

![](image_2022-11-22_02-02-45.png)
![](image_2022-11-22_02-03-01.png)